/*
* This file is part of Project SkyFire https://www.projectskyfire.org.
* See LICENSE.md file for Copyright information
*/

#include "ProcessDefines.h"

ServerProcessTypes SkyFire::Impl::CurrentServerProcessHolder::_type = NUM_SERVER_PROCESS_TYPES;
